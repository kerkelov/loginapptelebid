﻿using LoginAppTelebid.Framework.Http;
using LoginAppTelebid.Framework.Mvc.Attributes;
using LoginAppTelebid.Framework.Mvc.Contracts;
using static LoginAppTelebid.GlobalConstants;
using System.Reflection;

namespace LoginAppTelebid.Framework.Mvc;

public static class Host
{
    public static async Task CreateHostAsync(IMvcApplication application, int port = WebServerPort)
    {
        var routeTable = new List<Route>();
        var serviceCollection = new ServiceCollection();

        application.ConfigureServices(serviceCollection);
        application.Configure(routeTable);

        RegisterAssets(routeTable);
        RegisterRoutes(routeTable, application, serviceCollection);

        Console.WriteLine("Registered routes:");

        Console.WriteLine();
        foreach (var route in routeTable)
        {
            Console.WriteLine($"{route.Method} {route.Path}");
        }
        Console.WriteLine();

        var server = new HttpServer(routeTable);
        await server.StartAsync(port);
    }

    private static void RegisterRoutes(
        IList<Route> routeTable,
        IMvcApplication application,
        IServiceCollection serviceCollection)
    {
        var controllerTypes = application
            .GetType()
            .Assembly
            .GetTypes()
            .Where(type => type.IsClass &&
                !type.IsAbstract &&
                type.IsSubclassOf(typeof(Controller))
            );

        foreach (var controllerType in controllerTypes)
        {
            var methods = controllerType
                .GetMethods()
                .Where(method =>
                    method.IsPublic &&
                    !method.IsStatic &&
                    method.DeclaringType == controllerType &&
                    !method.IsAbstract &&
                    !method.IsConstructor &&
                    !method.IsSpecialName
                );

            foreach (var method in methods)
            {
                var controllerName = controllerType.Name.Replace("Controller", string.Empty);
                var url = $"/{controllerName}/{method.Name}";

                var attribute = method
                    .GetCustomAttributes(false)
                    .Where(attributes => attributes
                        .GetType()
                        .IsSubclassOf(typeof(BaseHttpAttribute))
                    )
                    .FirstOrDefault() as BaseHttpAttribute;

                var httpMethod = HttpMethod.Get;

                if (attribute != null)
                {
                    httpMethod = attribute.Method;
                }

                if (!string.IsNullOrEmpty(attribute?.Url))
                {
                    url = attribute.Url;
                }

                var route = new Route(
                    url,
                    httpMethod,
                    request => ExecuteAction(request, controllerType, method, serviceCollection)
                );

                routeTable.Add(route);
            }
        }
    }

    private static HttpResponse ExecuteAction(
        HttpRequest request,
        Type controllerType,
        MethodInfo action,
        IServiceCollection serviceCollection
    )
    {
        var instance = serviceCollection.CreateInstance(controllerType) as Controller;
        instance.Request = request;

        var arguments = new List<object>();
        var parameters = action.GetParameters();

        foreach (var parameter in parameters)
        {
            var HttpParamerValue = GetParameterFromRequest(request, parameter.Name);
            var parameterValue = Convert.ChangeType(HttpParamerValue, parameter.ParameterType);

            if (parameterValue == null &&
                parameter.ParameterType != typeof(string) &&
                parameter.ParameterType != typeof(int?)
            )
            {
                parameterValue = Activator.CreateInstance(parameter.ParameterType);
                var properties = parameter.ParameterType.GetProperties();

                foreach (var property in properties)
                {
                    var propertyHttpParamerValue = GetParameterFromRequest(request, property.Name);
                    var propertyParameterValue = Convert.ChangeType(propertyHttpParamerValue, property.PropertyType);

                    property.SetValue(parameterValue, propertyParameterValue);
                }
            }

            arguments.Add(parameterValue);
        }

        var response = action.Invoke(instance, arguments.ToArray()) as HttpResponse;
        return response;
    }

    private static string GetParameterFromRequest(HttpRequest request, string parameterName)
    {
        var kvpDefaultValue = default(KeyValuePair<string, string>);
        parameterName = parameterName.ToLower();

        var parameterFromFormData = request
            .FormData
            .FirstOrDefault(entry => entry.Key.ToLower() == parameterName);

        if (!parameterFromFormData.Equals(kvpDefaultValue))
        {
            return parameterFromFormData.Value;
        }

        var parameterFromQuery = request
            .QueryData
            .FirstOrDefault(entry => entry.Key.ToLower() == parameterName);

        if (!parameterFromQuery.Equals(kvpDefaultValue))
        {
            return parameterFromQuery.Value;
        }

        return null;
    }

    private static void RegisterAssets(IList<Route> routeTable)
    {
        var staticFiles = Directory.GetFiles($"{AssetsFolderPath}", "*", SearchOption.AllDirectories);

        foreach (var staticFile in staticFiles)
        {
            var url = staticFile
                .Replace(AssetsFolderPath, string.Empty)
                .Replace("\\", "/");

            var route = new Route(url, HttpMethod.Get, (request) =>
            {
                var fileContent = File.ReadAllBytes(staticFile);
                var fileExtension = new FileInfo(staticFile).Extension;
                var contentType = fileExtension switch
                {
                    ".txt" => "text/plain",
                    ".js" => "text/javascript",
                    ".css" => "text/css",
                    ".jpg" => "image/jpg",
                    ".jpeg" => "image/jpg",
                    ".png" => "image/png",
                    ".gif" => "image/gif",
                    ".ico" => "image/vnd.microsoft.icon",
                    ".html" => "text/html",
                    _ => "text/plain",
                };

                return new HttpResponse(contentType, fileContent, HttpStatusCode.Ok);
            });

            routeTable.Add(route);
        }
    }
}
