﻿using LoginAppTelebid.Framework.Http;
using LoginAppTelebid.Framework.Mvc.Contracts;
using static LoginAppTelebid.GlobalConstants;
using System.Text;

namespace LoginAppTelebid.Framework.Mvc;

public abstract class Controller
{
    private readonly IViewEngine viewEngine;

    public Controller() => this.viewEngine = new ViewEngine();

    protected HttpResponse View(string viewPath, object viewModel = null)
    {
        var controllerName = this.GetType().Name.Replace("Controller", string.Empty);
        var viewContent = File.ReadAllText($"{ViewsFolderPath}/{controllerName}/{viewPath}.html");

        var html = viewEngine.GetHtml(viewContent, viewModel);
        var responseBodyBytes = Encoding.UTF8.GetBytes(html);
        var response = new HttpResponse("text/html", responseBodyBytes);

        return response;
    }

    public HttpRequest Request { get; set; }

    protected HttpResponse Redirect(string url)
    {
        var response = new HttpResponse(HttpStatusCode.Found);
        response.Headers.Add(new Header("Location", url));

        return response;
    }

    protected HttpResponse InternalServerError(string message = "Internal server error")
    {
        var responseBodyBytes = Encoding.UTF8.GetBytes(message);
        var response = new HttpResponse("application/json", responseBodyBytes, HttpStatusCode.ServerError);

        return response;
    }

    protected HttpResponse BadRequest(string message = "Bad request")
    {
        var responseBodyBytes = Encoding.UTF8.GetBytes(message);
        var response = new HttpResponse("application/json", responseBodyBytes, HttpStatusCode.BadRequest);

        return response;
    }

    protected HttpResponse Created(string message = "Created")
    {
        var responseBodyBytes = Encoding.UTF8.GetBytes(message);
        var response = new HttpResponse("application/json", responseBodyBytes, HttpStatusCode.Created);

        return response;
    }

    protected HttpResponse NotFound(string message = "Not found")
    {
        var responseBodyBytes = Encoding.UTF8.GetBytes(message);
        var response = new HttpResponse("application/json", responseBodyBytes, HttpStatusCode.NotFound);

        return response;
    }

    protected HttpResponse Ok(string message = "Ok")
    {
        var responseBodyBytes = Encoding.UTF8.GetBytes(message);
        var response = new HttpResponse("application/json", responseBodyBytes, HttpStatusCode.Ok);

        return response;
    }

    protected HttpResponse Unauthorized(string message = "Unauthorized")
    {
        var responseBodyBytes = Encoding.UTF8.GetBytes(message);
        var response = new HttpResponse("application/json", responseBodyBytes, HttpStatusCode.Unauthorized);

        return response;
    }
}
