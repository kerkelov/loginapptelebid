﻿using LoginAppTelebid.Framework.Mvc.Contracts;

namespace LoginAppTelebid.Framework.Mvc;

public class ViewEngine : IViewEngine
{
    public string GetHtml(string templateCode, object viewModel)
    {
        if (viewModel == null)
        {
            return templateCode;
        }

        var properties = viewModel.GetType().GetProperties();

        foreach (var property in properties)
        {
            var name = property.Name;
            var value = property.GetValue(viewModel);

            templateCode = templateCode.Replace($"@{name}@", value.ToString());
        }

        return templateCode;
    }
}
