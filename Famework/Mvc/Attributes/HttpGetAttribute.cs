﻿using LoginAppTelebid.Framework.Http;

namespace LoginAppTelebid.Framework.Mvc.Attributes;

public class HttpGetAttribute : BaseHttpAttribute
{
    public HttpGetAttribute()
    {
    }

    public HttpGetAttribute(string url) => this.Url = url;

    public override HttpMethod Method => HttpMethod.Get;
}
