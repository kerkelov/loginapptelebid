﻿using LoginAppTelebid.Framework.Http;

namespace LoginAppTelebid.Framework.Mvc.Attributes
{
    public class HttpDeleteAttribute : BaseHttpAttribute
    {
        public HttpDeleteAttribute()
        {
        }

        public HttpDeleteAttribute(string url) => this.Url = url;

        public override HttpMethod Method => HttpMethod.Delete;
    }
}
