﻿using LoginAppTelebid.Framework.Http;

namespace LoginAppTelebid.Framework.Mvc.Attributes
{
    public class HttpPutAttribute : BaseHttpAttribute
    {
        public HttpPutAttribute()
        {
        }

        public HttpPutAttribute(string url) => this.Url = url;

        public override HttpMethod Method => HttpMethod.Put;
    }
}
