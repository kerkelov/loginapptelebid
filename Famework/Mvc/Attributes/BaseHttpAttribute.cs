﻿using LoginAppTelebid.Framework.Http;

namespace LoginAppTelebid.Framework.Mvc.Attributes;

public abstract class BaseHttpAttribute : Attribute
{
    public string Url { get; set; }

    public abstract HttpMethod Method { get; }
}
