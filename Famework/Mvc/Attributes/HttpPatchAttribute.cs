﻿using LoginAppTelebid.Framework.Http;

namespace LoginAppTelebid.Framework.Mvc.Attributes
{
    public class HttpPatchAttribute : BaseHttpAttribute
    {
        public HttpPatchAttribute()
        {
        }

        public HttpPatchAttribute(string url) => this.Url = url;

        public override HttpMethod Method => HttpMethod.Patch;
    }
}
