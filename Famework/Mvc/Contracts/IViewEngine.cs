﻿namespace LoginAppTelebid.Framework.Mvc.Contracts;

public interface IViewEngine
{
    string GetHtml(string templateCode, object viewModel);
}
