﻿
namespace LoginAppTelebid.Framework.Mvc.Contracts;

public interface IServiceCollection
{
    void Add<TSource, TDestination>();

    object CreateInstance(Type type);

    TType CreateInstance<TType>();
}
