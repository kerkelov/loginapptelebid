﻿using LoginAppTelebid.Framework.Http;

namespace LoginAppTelebid.Framework.Mvc.Contracts;

public interface IMvcApplication
{
    void ConfigureServices(IServiceCollection serviceCollection);

    void Configure(List<Route> routeTable);
}
