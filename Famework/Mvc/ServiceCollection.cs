﻿using LoginAppTelebid.Framework.Mvc.Contracts;

namespace LoginAppTelebid.Framework.Mvc;

public class ServiceCollection : IServiceCollection
{
    private Dictionary<Type, Type> dependencyContainer = new();

    public void Add<TSource, TDestination>()
        => this.dependencyContainer[typeof(TSource)] = typeof(TDestination);

    public object CreateInstance(Type type)
    {
        if (this.dependencyContainer.ContainsKey(type))
        {
            type = this.dependencyContainer[type];
        }

        var constructor = type
            .GetConstructors()
            .OrderBy(x => x.GetParameters().Count())
            .FirstOrDefault();

        var parameters = constructor.GetParameters();
        var parameterValues = new List<object>();

        foreach (var parameter in parameters)
        {
            var parameterValue = CreateInstance(parameter.ParameterType);
            parameterValues.Add(parameterValue);
        }

        var instance = constructor.Invoke(parameterValues.ToArray());

        return instance;
    }

    public TType CreateInstance<TType>()
        => (TType)this.CreateInstance(typeof(TType));
}
