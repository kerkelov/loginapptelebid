﻿using LoginAppTelebid.Framework.Extensions;

namespace LoginAppTelebid.Framework.Http;

public class Header
{
    public Header(string name, string value)
        => (this.Name, this.Value) = (name, value);

    public Header(string headerLine)
        => (this.Name, this.Value) = headerLine.Split([": "], 2, StringSplitOptions.None);

    public override string ToString()
        => $"{this.Name}: {this.Value}";

    public string Name { get; set; }

    public string Value { get; set; }
}
