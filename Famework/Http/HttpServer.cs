﻿using LoginAppTelebid.Framework.Extensions;
using System.Net;
using System.Net.Sockets;
using System.Text;
using static LoginAppTelebid.Framework.Http.HttpConstants;

namespace LoginAppTelebid.Framework.Http;

public class HttpServer(List<Route> routeTable) : IHttpServer
{
    public async Task StartAsync(int port)
    {
        var tcpListener = new TcpListener(IPAddress.Loopback, port);
        tcpListener.Start();

        Console.WriteLine($"Listening at {IPAddress.Loopback}:{port}");
        Console.WriteLine();

        while (true)
        {
            var tcpClient = await tcpListener.AcceptTcpClientAsync();
            ProcessClientAsync(tcpClient);
        }
    }

    private async Task ProcessClientAsync(TcpClient tcpClient)
    {
        try
        {
            using var stream = tcpClient.GetStream();
            byte[] buffer = new byte[BufferSize];
            int bytesRead;

            while ((bytesRead = await stream.ReadAsync(buffer, 0, buffer.Length)) > 0)
            {
                var requestAsString = Encoding.UTF8.GetString(buffer, 0, bytesRead);
                var request = new HttpRequest(requestAsString);

                Console.WriteLine($"{request.Method} {request.Path} => {request.Headers.Count} headers");

                var response = new HttpResponse("text/html", [], HttpStatusCode.NotFound);

                var route = routeTable.FirstOrDefault(route =>
                    route.Path.EqualsIgnoreCase(request.Path) &&
                    route.Method == request.Method
                );

                if (route != null)
                {
                    response = route.Action(request);
                }

                response.Headers.Add(new Header("Server", "LoginAppTelebid Server 1.0"));

                var responseHeaderBytes = Encoding.UTF8.GetBytes(response.ToString());
                await stream.WriteAsync(responseHeaderBytes, 0, responseHeaderBytes.Length);

                if (response.Body != null)
                {
                    await stream.WriteAsync(response.Body, 0, response.Body.Length);
                }
            }
        }
        catch (Exception ex)
        {
            Console.WriteLine(ex);
        }
        finally
        {
            tcpClient.Close();
        }
    }
}
