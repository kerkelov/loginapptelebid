﻿namespace LoginAppTelebid.Framework.Http;

public interface IHttpServer
{
    Task StartAsync(int port);
}
