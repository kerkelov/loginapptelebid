﻿namespace LoginAppTelebid.Framework.Http;

public enum HttpMethod
{
    Get = 1,
    Post = 2,
    Patch = 3,
    Put = 4,
    Delete = 5,
}
