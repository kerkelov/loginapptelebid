﻿using System.Text;

namespace LoginAppTelebid.Framework.Http;

public class ResponseCookie : Cookie
{
    public ResponseCookie(string name, string value) : base(name, value)
        => this.Path = "/";

    public override string ToString()
    {
        var cookieBuilder = new StringBuilder($"{this.Name}={this.Value}; Path={this.Path};");

        if (MaxAge != 0)
        {
            cookieBuilder.Append($" Max-Age={this.MaxAge};");
        }

        if (this.HttpOnly)
        {
            cookieBuilder.Append(" HttpOnly;");
        }

        return cookieBuilder.ToString();
    }

    public int MaxAge { get; set; }

    public bool HttpOnly { get; set; }

    public string Path { get; set; }
}
