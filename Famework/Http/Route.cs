﻿namespace LoginAppTelebid.Framework.Http;

public class Route(string path, HttpMethod method, Func<HttpRequest, HttpResponse> action)
{
    public string Path { get; set; } = path;

    public HttpMethod Method { get; set; } = method;

    public Func<HttpRequest, HttpResponse> Action { get; set; } = action;
}
