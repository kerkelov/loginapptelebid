﻿namespace LoginAppTelebid.Framework.Http;

public enum HttpStatusCode
{
    Ok = 200,
    MovedPermanently = 301,
    Found = 302,
    TemporaryRedirect = 307,
    NotFound = 404,
    ServerError = 500,
    Unauthorized = 401,
    BadRequest = 400,
    Created = 201
}
