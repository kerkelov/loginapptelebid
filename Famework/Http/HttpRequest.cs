﻿using System.Net;
using System.Text;
using LoginAppTelebid.Framework.Extensions;
using static LoginAppTelebid.Framework.Http.HttpConstants;

namespace LoginAppTelebid.Framework.Http;

public class HttpRequest
{
    public HttpRequest(string requestString)
    {
        var lines = requestString.Split(NewLine, StringSplitOptions.None);

        var headerLine = lines[0];
        var headerLineParts = headerLine.Split(' ');

        this.Method = headerLineParts[0].ToEnum<HttpMethod>();
        this.Path = headerLineParts[1];

        int lineIndex = 1;
        bool isInHeaders = true;
        StringBuilder bodyBuilder = new StringBuilder();

        while (lineIndex < lines.Length)
        {
            var line = lines[lineIndex];
            lineIndex++;

            if (string.IsNullOrWhiteSpace(line))
            {
                isInHeaders = false;
                continue;
            }

            if (isInHeaders)
            {
                this.Headers.Add(new Header(line));
            }
            else
            {
                bodyBuilder.AppendLine(line);
            }
        }

        var cookiesAsString = this.Headers
            .FirstOrDefault(header => header.Name == RequestCookieHeader)
            ?.Value;

        if (cookiesAsString != null)
        {
            var cookies = cookiesAsString.Split("; ", StringSplitOptions.RemoveEmptyEntries);

            foreach (var cookieAsString in cookies)
            {
                this.Cookies.Add(new Cookie(cookieAsString));
            }
        }

        var sessionCookie = this.Cookies.FirstOrDefault(cookie => cookie.Name == SessionCookieName);

        if (sessionCookie != null)
        {
            this.Session = sessionCookie.Value;
        }

        if (this.Path.Contains("?"))
        {
            var pathParts = this.Path.Split('?', 2);

            this.Path = pathParts[0];
            this.QueryString = pathParts[1];
        }
        else
        {
            this.QueryString = string.Empty;
        }

        this.Body = bodyBuilder.ToString().TrimEnd('\n', '\r');

        SplitParameters(this.Body, this.FormData);
        SplitParameters(this.QueryString, this.QueryData);
    }

    private static void SplitParameters(string parametersAsString, IDictionary<string, string> output)
    {
        var parameters = parametersAsString.Split('&', StringSplitOptions.RemoveEmptyEntries);

        foreach (var parameter in parameters)
        {
            var parameterParts = parameter.Split('=', 2);

            var name = parameterParts[0];
            var value = WebUtility.UrlDecode(parameterParts[1]);

            if (!output.ContainsKey(name))
            {
                output.Add(name, value);
            }
        }
    }

    public string Path { get; set; }

    public string QueryString { get; set; }

    public HttpMethod Method { get; set; }

    public ICollection<Header> Headers { get; set; } = new List<Header>();

    public ICollection<Cookie> Cookies { get; set; } = new List<Cookie>();

    public IDictionary<string, string> FormData { get; set; } = new Dictionary<string, string>();

    public IDictionary<string, string> QueryData { get; set; } = new Dictionary<string, string>();

    public string Session { get; set; }

    public string Body { get; set; }
}
