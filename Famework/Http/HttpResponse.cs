﻿using System.Text;
using static LoginAppTelebid.Framework.Http.HttpConstants;

namespace LoginAppTelebid.Framework.Http;

public class HttpResponse
{
    public HttpResponse(HttpStatusCode statusCode)
        => this.StatusCode = statusCode;

    public HttpResponse(string contentType, byte[] body, HttpStatusCode statusCode = HttpStatusCode.Ok)
    {
        ArgumentNullException.ThrowIfNull(body, nameof(body));

        this.StatusCode = statusCode;
        this.Body = body;
        this.Cookies = [];
        this.Headers = [
             new Header("Content-Type", contentType),
             new Header("Content-Length", body.Length.ToString()),
        ];
    }

    public override string ToString()
    {
        var responseBuilder = new StringBuilder(
            $"Http/1.1 {(int)this.StatusCode} {this.StatusCode}" + NewLine
        );

        foreach (var header in this.Headers)
        {
            responseBuilder.Append(header.ToString() + NewLine);
        }

        foreach (var cookie in this.Cookies)
        {
            responseBuilder.Append("Set-Cookie: " + cookie.ToString() + NewLine);
        }

        responseBuilder.Append(NewLine);

        return responseBuilder.ToString();
    }

    public HttpStatusCode StatusCode { get; set; }

    public ICollection<Header> Headers { get; set; } = new List<Header>();

    public ICollection<Cookie> Cookies { get; set; } = new List<Cookie>();

    public byte[] Body { get; set; }
}
