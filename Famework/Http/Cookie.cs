﻿using LoginAppTelebid.Framework.Extensions;

namespace LoginAppTelebid.Framework.Http;

public class Cookie
{
    public Cookie(string name, string value)
        => (this.Name, this.Value) = (name, value);

    public Cookie(string cookieAsString)
        => (this.Name, this.Value) = cookieAsString.Split('=', 2);

    public override string ToString()
           => $"{this.Name}={this.Value}";

    public string Name { get; set; }

    public string Value { get; set; }
}