namespace LoginAppTelebid.Framework.Extensions;

public static class ArrayExtensions
{
    public static void Deconstruct<T>(this T[] array, out T first, out T second)
    {
        if (array.Length < 2)
        {
            throw new ArgumentException("You are trying to deconstruct an array with less than two elements");
        }

        first = array[0];
        second = array[1];
    }
}