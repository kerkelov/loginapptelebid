using LoginAppTelebid.Framework.Http;

namespace LoginAppTelebid.Framework.Extensions;

public static class StringExtensions
{
    public static TEnum ToEnum<TEnum>(this string text)
        => (TEnum)Enum.Parse(typeof(TEnum), text, true);

    public static bool EqualsIgnoreCase(this string text, string anotherText)
        => text.ToLower() == anotherText.ToLower();
}