
async function handleRegistration(response) {
    let statusCode = +response.status,
        body = await response.text();

    if (statusCode == 201) {
        alert("Please open the email we send you to verify your account.")
    } else {
        alert(body);
    }
}

async function handleLogin(response) {
    let statusCode = +response.status,
        body = await response.text();

    if (statusCode === 200) {
        document.body.innerHTML = body;
    } else {
        alert(body);
    }
}