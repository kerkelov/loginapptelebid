
async function handleLogout() {
    let response = await fetch('/', {
        method: 'POST'
    });

    let statusCode = response.status,
        body = await response.text();

    console.log(statusCode, body);
}

async function handleUpdate(response) {
    let statusCode = +response.status,
        body = await response.text();

    if (statusCode === 200) {
        alert("Information updated successfully")
    } else {
        alert(body);
    }

    refreshHomePage();
}

async function refreshHomePage() {
    let response = await fetch('/'),
        body = await response.text();

    document.body.innerHTML = body;
}
