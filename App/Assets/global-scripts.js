async function submitForm(formId, path, onCompleted, method = 'POST') {
    let form = document.getElementById(formId),
        formData = new FormData(form);

    let response = await fetch(path, {
        method,
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded',
        },
        body: new URLSearchParams(formData),
    });

    onCompleted(response);
}
