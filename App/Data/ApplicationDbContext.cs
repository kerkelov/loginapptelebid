using Microsoft.EntityFrameworkCore;

namespace LoginAppTelebid.Data;

public class ApplicationDbContext : DbContext
{
    private const string DatabaseName = "LoginFormTelebid";
    public static string DbPath { get; }

    static ApplicationDbContext()
    {
        var folder = Environment.SpecialFolder.LocalApplicationData;
        var path = Environment.GetFolderPath(folder);
        DbPath = Path.Join(path, $"{DatabaseName}.db");

        Console.WriteLine("The database is SQLite and is stored here: ");
        Console.WriteLine(DbPath);
        Console.WriteLine();
    }

    public ApplicationDbContext()
    {
    }

    public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
        : base(options)
    {
    }

    public DbSet<User> Users { get; set; }

    public DbSet<AccountConfirmation> AccountConfirmations { get; set; }

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        => optionsBuilder.UseSqlite($"Data Source={DbPath}");
}