using System.ComponentModel.DataAnnotations;

namespace LoginAppTelebid.Data;

public class AccountConfirmation
{
    public AccountConfirmation() => this.Id = Guid.NewGuid().ToString();

    public string Id { get; set; }

    [Required]
    public string UserId { get; set; }

    public User User { get; set; }
}