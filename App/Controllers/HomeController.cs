using LoginAppTelebid.Framework.Http;
using LoginAppTelebid.Framework.Mvc;
using LoginAppTelebid.Framework.Mvc.Attributes;
using LoginAppTelebid.Services;
using LoginAppTelebid.Services.Contracts;

namespace LoginAppTelebid.Controllers;

public class HomeController(
    IUsersService usersService,
    IEmailSender emailSender,
    ISessionManager sessionManager
) : Controller
{
    [HttpGet("/")]
    public HttpResponse ShowLoginOrProfilePage()
    {
        var userId = sessionManager.GetSession(this.Request.Session);

        if (userId != null)
        {
            var viewModel = usersService.GetById(userId);
            return this.View("profile-page", viewModel);
        }
        else
        {
            return this.View("login-page");
        }
    }
}
