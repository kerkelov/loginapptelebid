using LoginAppTelebid.Framework.Http;
using LoginAppTelebid.Framework.Mvc;
using LoginAppTelebid.Framework.Mvc.Attributes;
using LoginAppTelebid.Services.Contracts;
using static LoginAppTelebid.Framework.Http.HttpConstants;

namespace LoginAppTelebid.Controllers;

public class AuthController(
    IUsersService usersService,
    IEmailSender emailSender,
    ISessionManager sessionManager,
    IAccountConfirmationManager accountConfirmationManager
) : Controller
{
    [HttpPost]
    public HttpResponse Login(string email, string password)
    {
        var user = usersService.GetByEmail(email);

        if (user == null || !usersService.CheckPassword(user.Id, password))
        {
            return this.Unauthorized("Invalid credentials");
        }

        if (!user.IsEmailConfirmed)
        {
            return this.Unauthorized("You need to confirm your account first!");
        }


        var response = this.Redirect("/");
        var sessionId = sessionManager.CreateSession(user.Id);
        var cookie = new ResponseCookie(SessionCookieName, sessionId);
        response.Cookies.Add(cookie);


        return response;
    }

    [HttpPost]
    public HttpResponse Logout()
    {
        var success = sessionManager.DeleteSession(this.Request.Session);

        if (!success)
        {
            return BadRequest("This session id doesn't exist");
        }

        return this.Redirect("/");
    }

    [HttpPost]
    public HttpResponse Register(string username, string email, string password, string description)
    {
        var errorMessage = usersService.CreateUser(username, email, password, description);
        var isCreated = string.IsNullOrEmpty(errorMessage);

        if (!isCreated)
        {
            return this.BadRequest(errorMessage);
        }

        var userId = usersService.GetByEmail(email).Id;
        emailSender.SendConfirmationEmail(email, userId);

        return this.Created();
    }

    [HttpGet]
    public HttpResponse ConfirmAccount(string token)
    {
        var confirmed = accountConfirmationManager.Confirm(token);

        if (!confirmed)
        {
            return this.BadRequest("Invalid account confirmation token");
        }

        return this.View("account-confirmed");
    }
}