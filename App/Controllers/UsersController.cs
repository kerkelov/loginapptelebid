using LoginAppTelebid.Framework.Http;
using LoginAppTelebid.Framework.Mvc;
using LoginAppTelebid.Framework.Mvc.Attributes;
using LoginAppTelebid.Services;
using LoginAppTelebid.Services.Contracts;

namespace LoginAppTelebid.Controllers;

public class UsersController(
    IUsersService usersService
) : Controller
{
    [HttpPut]
    public HttpResponse Update(string id, string username, string email, string description)
    {
        var errorMessage = usersService.Update(id, username, email, description);

        if (errorMessage != null)
        {
            return this.BadRequest(errorMessage);
        }

        return this.Ok();
    }
}
