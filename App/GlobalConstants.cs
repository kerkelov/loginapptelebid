﻿namespace LoginAppTelebid;
public static class GlobalConstants
{
    public const string AppName = "LoginAppTelebid";
    public const int WebServerPort = 5379;

    public static readonly string AppDirectory =
            Environment.CurrentDirectory.Replace(@"\bin\Debug\net8.0", string.Empty);

    public static readonly string AssetsFolderPath = $"{AppDirectory}/App/Assets";
    public static readonly string ViewsFolderPath = $"{AppDirectory}/App/Views";
    public static readonly string ControllersFolderPath = $"{AppDirectory}/App/Controllers";
}
