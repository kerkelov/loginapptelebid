using System.Net.Mail;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using LoginAppTelebid.Data;
using LoginAppTelebid.Services.Contracts;
using LoginAppTelebid.Services.Models;
using System.Text.RegularExpressions;

namespace LoginAppTelebid.Services;

public class UsersService(ApplicationDbContext db) : IUsersService
{
    public string CreateUser(string username, string email, string password, string description)
    {
        //Must be at least 6 characters long with a number and special symbol
        string passwordRegex = "^(?=.*?[A-Za-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-+_(){}<>,;]).{6,}$";

        if (!Regex.IsMatch(password, passwordRegex))
        {
            return "Password must be at least 6 characters long with a number and special symbol";
        }

        if (!this.IsEmailAvailable(email))
        {
            return "This email already exists";
        }

        if (!isEmailValid(email))
        {
            return "Invalid email";
        }

        if (string.IsNullOrEmpty(username))
        {
            return "Username cannot be empty";
        }

        var user = new User
        {
            Email = email,
            Username = username,
            Description = description,
            Password = ComputeHash(password),
        };

        db.Users.Add(user);
        db.SaveChanges();

        return null;
    }

    public UserModel GetById(string id) =>
        this.GetUser(user => user.Id == id);

    public bool IsEmailAvailable(string email)
        => db.Users.Any(user => user.Email == email) == false;

    public UserModel GetByEmail(string email)
        => this.GetUser(user => user.Email == email);

    public string Update(string id, string username, string email, string description)
    {
        var user = db.Users.FirstOrDefault(user => user.Id == id);

        if (user == null)
        {
            return "User with this id does not exist";
        }

        var isThereAnotherUserWithThisEmail = db.Users
            .Any(user => user.Email == email && user.Id != id);

        if (isThereAnotherUserWithThisEmail)
        {
            return "This email already exists";
        }

        if (!isEmailValid(email))
        {
            return "Invalid email";
        }

        if (string.IsNullOrEmpty(username))
        {
            return "Username cannot be empty";
        }

        user.Username = username;
        user.Email = email;
        user.Description = description;
        db.SaveChanges();

        return null;
    }

    public bool CheckPassword(string userId, string passwordText)
    {
        var user = db.Users.FirstOrDefault(user => user.Id == userId);

        if (user != null && ComputeHash(passwordText) == user.Password)
        {
            return true;
        }

        return false;
    }

    private UserModel GetUser(Func<User, bool> predicate)
    {
        var user = db.Users.FirstOrDefault(predicate);

        if (user == null)
        {
            return null;
        }

        var userModel = new UserModel
        {
            Username = user.Username,
            Description = user.Description,
            Email = user.Email,
            IsEmailConfirmed = user.IsEmailConfirmed,
            Id = user.Id,
        };

        return userModel;
    }

    private static string ComputeHash(string input)
    {
        using var hash = SHA512.Create();

        var bytes = Encoding.UTF8.GetBytes(input);
        var hashedInputBytes = hash.ComputeHash(bytes);
        var hashedInputStringBuilder = new StringBuilder(128);

        foreach (var @byte in hashedInputBytes)
        {
            hashedInputStringBuilder.Append(@byte.ToString("X2"));
        }

        return hashedInputStringBuilder.ToString();
    }

    private static bool isEmailValid(string email)
    {
        try
        {
            email = new MailAddress(email).Address;
            return true;
        }
        catch (FormatException)
        {
            return false;
        }
    }
}
