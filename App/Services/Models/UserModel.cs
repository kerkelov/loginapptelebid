namespace LoginAppTelebid.Services.Models;

public class UserModel
{
    public UserModel() => this.Id = Guid.NewGuid().ToString();

    public string Id { get; set; }

    public string Username { get; set; }

    public string Email { get; set; }

    public string Description { get; set; }

    public bool IsEmailConfirmed { get; set; }
}