using System.Net;
using System.Net.Mail;
using LoginAppTelebid.Services.Contracts;

namespace LoginAppTelebid.Services;

public class EmailSender(IAccountConfirmationManager accountConfirmationManager) : IEmailSender
{
    public void SendConfirmationEmail(string recipientEmail, string userId)
    {
        const string senderEmail = "gkerkelov030@gmail.com";
        const string senderEmailUsername = senderEmail;
        const string senderEmailPassword = "nqrv qusn dydc dfqd";

        var content = $"""
            Click on this link to confirm your account:
            {accountConfirmationManager.GenerateConfirmationLink(userId)}     
        """;
        var emailToSend = new MailMessage(senderEmail, recipientEmail, "Confirm your account", content);

        var smtp = new SmtpClient
        {
            Host = "smtp.gmail.com",
            Port = 587,
            UseDefaultCredentials = false,
            Credentials = new NetworkCredential(senderEmailUsername, senderEmailPassword),
            EnableSsl = true,
            DeliveryMethod = SmtpDeliveryMethod.Network,
        };

        try
        {
            smtp.Send(emailToSend);
        }
        catch (SmtpException ex)
        {
            Console.WriteLine(ex.ToString());
        }
    }
}
