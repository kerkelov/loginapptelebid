
namespace LoginAppTelebid.Services.Contracts;

public interface IAccountConfirmationManager
{
    string GenerateConfirmationLink(string userId);

    bool Confirm(string confirmationToken);
}