
namespace LoginAppTelebid.Services.Contracts;

public interface IEmailSender
{
    void SendConfirmationEmail(string to, string userId);
}