namespace LoginAppTelebid.Services.Contracts;

public interface ISessionManager
{
    bool IsSessionActive(string sessionId);

    string GetSession(string sessionId);

    bool DeleteSession(string sessionId);

    string CreateSession(string session);
}