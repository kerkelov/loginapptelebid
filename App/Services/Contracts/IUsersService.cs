using LoginAppTelebid.Services.Models;

namespace LoginAppTelebid.Services.Contracts;

public interface IUsersService
{
    string CreateUser(string username, string email, string password, string description);

    UserModel GetById(string id);

    UserModel GetByEmail(string email);

    string Update(string id, string username, string email, string description);

    bool IsEmailAvailable(string email);

    bool CheckPassword(string userId, string passwordText);

}