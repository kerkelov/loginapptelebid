using static LoginAppTelebid.GlobalConstants;
using LoginAppTelebid.Services.Contracts;
using LoginAppTelebid.Data;
using Microsoft.EntityFrameworkCore;
using System.Reflection.PortableExecutable;

namespace LoginAppTelebid.Services;

public class AccountConfirmationManager(ApplicationDbContext db) : IAccountConfirmationManager
{
    public bool Confirm(string confirmationToken)
    {
        var accountConfirmation = db.AccountConfirmations
            .Include(accountConfirmation => accountConfirmation.User)
            .FirstOrDefault(accountConfirmation => accountConfirmation.Id == confirmationToken);

        if (accountConfirmation == null)
        {
            return false;
        }

        accountConfirmation.User.IsEmailConfirmed = true;
        db.SaveChanges();

        return true;
    }

    public string GenerateConfirmationLink(string userId)
    {
        var accountConfirmation = new AccountConfirmation { UserId = userId };
        db.AccountConfirmations.Add(accountConfirmation);
        db.SaveChanges();

        return $"http://localhost:{WebServerPort}/Auth/ConfirmAccount?token={accountConfirmation.Id}";
    }
}
