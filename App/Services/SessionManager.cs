using LoginAppTelebid.Services.Contracts;

namespace LoginAppTelebid.Services;

public class SessionManager : ISessionManager
{
    private static Dictionary<string, string> map = new();

    public string CreateSession(string userId)
    {
        var sessionId = Guid.NewGuid().ToString();
        map.Add(sessionId, userId);
        return sessionId;
    }

    public bool DeleteSession(string sessionId)
    {
        if (string.IsNullOrEmpty(sessionId))
        {
            return false;
        }

        return map.Remove(sessionId);
    }

    public bool IsSessionActive(string sessionId)
    {
        if (string.IsNullOrEmpty(sessionId))
        {
            return false;
        }

        return map.ContainsKey(sessionId);
    }

    public string GetSession(string sessionId)
    {
        if (!this.IsSessionActive(sessionId))
        {
            return null;
        }

        return map[sessionId];
    }
}
