using LoginAppTelebid.Framework.Http;
using LoginAppTelebid.Framework.Mvc;
using LoginAppTelebid.Framework.Mvc.Contracts;
using LoginAppTelebid.Services;
using LoginAppTelebid.Data;
using LoginAppTelebid.Services.Contracts;
using Microsoft.EntityFrameworkCore;

await Host.CreateHostAsync(new App());

public class App : IMvcApplication
{
    public void Configure(List<Route> routeTable)
        => new ApplicationDbContext().Database.Migrate();

    public void ConfigureServices(IServiceCollection serviceCollection)
    {
        serviceCollection.Add<ISessionManager, SessionManager>();
        serviceCollection.Add<IAccountConfirmationManager, AccountConfirmationManager>();
        serviceCollection.Add<IEmailSender, EmailSender>();
        serviceCollection.Add<IUsersService, UsersService>();
    }
}
